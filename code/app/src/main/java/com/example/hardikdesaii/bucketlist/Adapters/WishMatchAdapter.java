package com.example.hardikdesaii.bucketlist.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hardikdesaii.bucketlist.Activities.WishMatch;
import com.example.hardikdesaii.bucketlist.R;

import java.util.ArrayList;

/**
 * Created by HardikDesaii on 16/05/17.
 */

public class WishMatchAdapter extends RecyclerView.Adapter<WishMatchAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<String> list;

    public WishMatchAdapter(Context context, ArrayList<String> list)
    {
        this.context = context;
        this.list = list;
    }

    @Override
    public WishMatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_wishmatch, parent, false);
        return new WishMatchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        final String email=list.get(position);
        holder.tv_wishMatch.setText(email);
        holder.btn_wishMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof WishMatch)
                {
                    ((WishMatch)context).contactPerson(email);
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }
     class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tv_wishMatch;
        private Button btn_wishMatch;
        public ViewHolder(View itemView)
        {
            super(itemView);
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
            tv_wishMatch=(TextView)itemView.findViewById(R.id.tv_wishMatch);
            btn_wishMatch=(Button)itemView.findViewById(R.id.btn_wishMatch);
            tv_wishMatch.setTypeface(tf);

        }
    }
}
