package com.example.hardikdesaii.bucketlist.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.hardikdesaii.bucketlist.Activities.Home;
import com.example.hardikdesaii.bucketlist.Activities.Login;
import com.example.hardikdesaii.bucketlist.Adapters.MyWishListAdapter;
import com.example.hardikdesaii.bucketlist.Adapters.SelectWishAdapter;
import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.DetectConnection;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EnterWishFragment extends Fragment
{
    private RecyclerView recyclerView;
    private SelectWishAdapter adapter;
    private ArrayList<String> wishList;
    private Button btn_addToWishList;
    private MaterialDialog progressDialog;

    private String[] wishNames={"Female casual shirts",
            "Female Trousers",
            "Female Western ",
            "Female Ethnic",
            "Girls Body Suites",
            "Girls Baby Dresses",
            "Baby boy body suits",
            "Baby boy ethnic",
            "Mens 2-piece suits",
            "Mens 3-piece suits",
            "Mens shirts",
            "Mens denims",
            "Mens tshirts"
    };

    public EnterWishFragment() {
        // Required empty public constructor
    }
    public static EnterWishFragment newInstance() {
        EnterWishFragment fragment = new EnterWishFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
        }
        wishList=new ArrayList<>();
        getWishes();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_enter_wish, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_selectWish);
        btn_addToWishList=(Button)view.findViewById(R.id.btn_addToWishList);
        btn_addToWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wish= Home.getCurrentWish();
                String email=Home.mEmail;
                if(!(wish.equals("null")))
                {
                    addToWishList(email,wish);
                }
                else
                {
                    Toast.makeText(getActivity(),"Please select a wish !",Toast.LENGTH_SHORT).show();
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new SelectWishAdapter(getActivity(),wishList);
        recyclerView.setAdapter(adapter);

        return view;
    }

    private void addToWishList(String email, String wish)
    {

        if(DetectConnection.checkInternetConnection(getActivity()))
        {
            progressDialog = new MaterialDialog.Builder(getActivity())
                    .content("Please wait...")
                    .progress(true, 0)
                    .show();


            // Start the ProgressDailog
            final OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
            RequestBody requestBody = new FormBody.Builder()
                    .add(BucketListConstants.CUSTOMER_EMAIL, email)
                    .add(BucketListConstants.WISH, wish)
                    .build();

            try {
                final Request request = new Request.Builder()
                        .url("https://poojathakor99.000webhostapp.com/BucketList/android_wishlist.php")
                        .post(requestBody)
                        .build();

                client.newCall(request).enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, final IOException e) {
                        Log.e("productFragement", "onFailure", e);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                // stop showing progress Bar
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(),
                                        getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                            }
                        });
                    } // onFailure ends here

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.e("response", " " + response);
                        Log.e("response.body", " " + response.body());

                        if (response.body() != null) {
                            final String result = response.body().string();
                            try {
                                final JSONObject jsonObj = new JSONObject(result);
                                Log.e("Inside onResponse Try", result);
                                final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                                final boolean auth=jsonObj.getBoolean(BucketListConstants.AUTH);

                                Log.e("Mstatus ", "" + mMessage);

                                // if mStatus matches the server status
                                if (auth==true)
                                {

                                    // show the message in either case
                                    getActivity().runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            // stop displaying Loader
                                            progressDialog.dismiss();

                                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();


                                        }
                                    });
                                } else
                                {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            // stop displaying Loader
                                            progressDialog.dismiss();
                                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }


                            } // try insde onResponse ends here
                            catch (final JSONException e) {
                                Log.e("productFragement", "Inside on Response try", e);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // stop displaying Loader
                                        progressDialog.dismiss();

                                        Toast.makeText(getActivity(), getResources()
                                                .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } // Catch inside onResponse ends here
                        } // if condition on request body ends here
                    } // onResponse ends here
                }); // CallBack Ends here
            }
            catch (Exception ex)
            {
                Log.e("SplashActivity", "ex", ex);
            }
        } // detect connection ends here
        else
        {
            Toast.makeText(getActivity(),"Check your internet connection !",Toast.LENGTH_LONG).show();
        }
    } // add to wishList ends here

    public void getWishes()
    {
        for(int i=0;i<12;i++)
        {
            wishList.add(wishNames[i]);
        }
    }


}
