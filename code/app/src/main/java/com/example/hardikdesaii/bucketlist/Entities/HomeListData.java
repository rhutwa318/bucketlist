package com.example.hardikdesaii.bucketlist.Entities;

/**
 * Created by HardikDesaii on 10/05/17.
 */

public class HomeListData
{
    private String title;
    private int image;

    public HomeListData() {
    }

    public HomeListData(String title, int image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
