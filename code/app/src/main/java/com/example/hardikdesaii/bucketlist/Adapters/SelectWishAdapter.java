package com.example.hardikdesaii.bucketlist.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hardikdesaii.bucketlist.Activities.Home;
import com.example.hardikdesaii.bucketlist.R;

import java.util.ArrayList;

/**
 * Created by HardikDesaii on 07/05/17.
 */

public class SelectWishAdapter extends RecyclerView.Adapter<SelectWishAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<String> list;

    public SelectWishAdapter(Context context, ArrayList<String> list)
    {
        this.context = context;
        this.list = list;
    }

    @Override
    public SelectWishAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_select_wish, parent, false);
        return new SelectWishAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SelectWishAdapter.ViewHolder holder, int position)
    {
        final String name=list.get(position);
        holder.title.setText(name);
        if(name.equals(Home.getCurrentWish()))
        {
          holder.fa_right.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.fa_right.setVisibility(View.INVISIBLE);
        }
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(context instanceof Home)
                {
                    Home.setCurrentWish(name);
                    notifyDataSetChanged();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView title,fa_right;
        private CardView cardview;
        private LinearLayout layout;
        public ViewHolder(View itemView)
        {
            super(itemView);
            Typeface webFont=Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome-webfont.ttf");
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");

            title=(TextView)itemView.findViewById(R.id.tv_cardSelectWish);
            fa_right=(TextView)itemView.findViewById(R.id.tv_checkSlectwish);
            cardview=(CardView)itemView.findViewById(R.id.cv_selectWish);
            layout=(LinearLayout)itemView.findViewById(R.id.ll_selectWish);
            fa_right.setTypeface(webFont);
            title.setTypeface(tf);
        }
    }
}
