package com.example.hardikdesaii.bucketlist.Utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by HardikDesaii on 12/05/17.
 */

public class DetectConnection
{
    public static boolean checkInternetConnection(Context context) {
        if (context != null) {
            ConnectivityManager con_manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            return con_manager.getActiveNetworkInfo() != null
                    && con_manager.getActiveNetworkInfo().isAvailable()
                    && con_manager.getActiveNetworkInfo().isConnected();
        }
        return true;
    }
}
