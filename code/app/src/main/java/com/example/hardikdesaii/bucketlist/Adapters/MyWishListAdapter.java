package com.example.hardikdesaii.bucketlist.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hardikdesaii.bucketlist.Activities.Home;
import com.example.hardikdesaii.bucketlist.Activities.WishMatch;
import com.example.hardikdesaii.bucketlist.R;

import java.util.ArrayList;

/**
 * Created by HardikDesaii on 07/05/17.
 */

public class MyWishListAdapter extends RecyclerView.Adapter<MyWishListAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<String> list;

    public MyWishListAdapter(Context context, ArrayList<String> list)
    {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyWishListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_mywishlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyWishListAdapter.ViewHolder holder, int position)
    {
        final String wishName=list.get(position);
        holder.title.setText(wishName);

        holder.fa_match.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(context, WishMatch.class);
                intent.putExtra("name",wishName.toLowerCase());
                context.startActivity(intent);
            }
        });
        holder.fa_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
            if(context instanceof Home)
            {
                ((Home) context).deleteWish(wishName,list);
                list.remove(wishName);
                notifyDataSetChanged();
            }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView title,fa_match,fa_delete;
        public ViewHolder(View itemView)
        {
            super(itemView);
            title=(TextView)itemView.findViewById(R.id.tv_cardMyWishList);
            fa_delete=(TextView)itemView.findViewById(R.id.tv_deleteMyWishList);
            fa_delete.setClickable(true);
            fa_match=(TextView)itemView.findViewById(R.id.tv_matchMyWishList);
            fa_match.setClickable(true);

            Typeface webFont=Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome-webfont.ttf");
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Regular.ttf");
            title.setTypeface(tf);
            fa_match.setTypeface(webFont);
            fa_delete.setTypeface(webFont);

        }
    }
}
