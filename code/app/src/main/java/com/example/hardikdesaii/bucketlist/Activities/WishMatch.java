package com.example.hardikdesaii.bucketlist.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.hardikdesaii.bucketlist.Adapters.WishMatchAdapter;
import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.DetectConnection;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WishMatch extends AppCompatActivity
{
    private String wishName;
    private RecyclerView recyclerView;
    private WishMatchAdapter adapter;
    private ArrayList<String> list;
    private String myEmail;
    private MaterialDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_match);
        myEmail= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_EMAIL);
        wishName=getIntent().getStringExtra("name");
        recyclerView=(RecyclerView)findViewById(R.id.rv_Wishmatch);
        LinearLayoutManager layoutManager = new LinearLayoutManager(WishMatch.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        list=new ArrayList<>();
        getNames();
        adapter=new WishMatchAdapter(WishMatch.this,list);
        recyclerView.setAdapter(adapter);

    }

    public void getNames()
    {
        if(DetectConnection.checkInternetConnection(WishMatch.this))
        {
            progressDialog = new MaterialDialog.Builder(WishMatch.this)
                    .content("Please wait...")
                    .progress(true, 0)
                    .show();


            // Start the ProgressDailog
            final OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
            RequestBody requestBody = new FormBody.Builder()
                    .add(BucketListConstants.WISH, wishName)
                    .build();

            try {
                final Request request = new Request.Builder()
                        .url("https://poojathakor99.000webhostapp.com/BucketList/android_wish_match.php")
                        .post(requestBody)
                        .build();

                client.newCall(request).enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, final IOException e) {
                        Log.e("productFragement", "onFailure", e);
                        WishMatch.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                // stop showing progress Bar
                                progressDialog.dismiss();
                                Toast.makeText(WishMatch.this,
                                        getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                            }
                        });
                    } // onFailure ends here

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.e("response", " " + response);
                        Log.e("response.body", " " + response.body());

                        if (response.body() != null) {
                            final String result = response.body().string();
                            try {
                                final JSONObject jsonObj = new JSONObject(result);
                                Log.e("Inside onResponse Try", result);
                                final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                                final boolean auth=jsonObj.getBoolean(BucketListConstants.AUTH);

                                Log.e("Mstatus ", "" + mMessage);

                                // if mStatus matches the server status
                                if (auth==true)
                                {

                                    JSONArray array=jsonObj.getJSONArray("user_list");
                                    for(int i=0;i<array.length();i++)
                                    {
                                        String name=array.get(i).toString();
                                        if(!(myEmail.equals(name)))
                                        {
                                            list.add(name);
                                        }

                                    }

                                    // show the message in either case
                                    WishMatch.this.runOnUiThread(new Runnable() {

                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();

                                            // stop displaying Loader
                                            adapter.notifyDataSetChanged();

                                        }
                                    });
                                } else
                                {

                                    WishMatch.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();

                                            // stop displaying Loader
                                            Toast.makeText(WishMatch.this, mMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }


                            } // try insde onResponse ends here
                            catch (final JSONException e) {
                                Log.e("productFragement", "Inside on Response try", e);
                                WishMatch.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();

                                        // stop displaying Loader
                                        Toast.makeText(WishMatch.this, getResources()
                                                .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } // Catch inside onResponse ends here
                        } // if condition on request body ends here
                    } // onResponse ends here
                }); // CallBack Ends here
            }
            catch (Exception ex)
            {
                Log.e("SplashActivity", "ex", ex);
            }
        } // detect connection ends here
        else
        {
            Toast.makeText(WishMatch.this,"Check your internet connection !",Toast.LENGTH_LONG).show();
        }
    } // getNames ends here

    public void contactPerson(String userEmail)
    {
        progressDialog = new MaterialDialog.Builder(WishMatch.this)
                .content("Please wait...")
                .progress(true, 0)
                .show();

        // Start the ProgressDailog
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        supportInvalidateOptionsMenu();
        RequestBody requestBody = new FormBody.Builder()
                .add("contact", userEmail)
                .add("sender", myEmail)
                .build();

        try {
            final Request request = new Request.Builder()
                    .url("https://poojathakor99.000webhostapp.com/BucketList/android_wish_email.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, final IOException e) {
                    Log.e("productFragement", "onFailure", e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // stop showing progress Bar
                            progressDialog.dismiss();
                            Toast.makeText(WishMatch.this,
                                    getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                        }
                    });
                } // onFailure ends here

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("response", " " + response);
                    Log.e("response.body", " " + response.body());

                    if (response.body() != null)
                    {
                        final String result = response.body().string();
                        try {

                                // show the message in either case
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();

                                        // stop displaying Loader
                                        // Take user to Menu activity
                                        Toast.makeText(WishMatch.this,"An email has been send , now you can communicate using email", Toast.LENGTH_SHORT).show();

                                    }
                                });

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();


                                        // stop displaying Loader
                                        Toast.makeText(WishMatch.this,"An email has been send , now you can communicate using email", Toast.LENGTH_SHORT).show();
                                    }
                                });



                        } // try insde onResponse ends here
                        catch (final Exception e) {
                            Log.e("productFragement", "Inside on Response try", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // stop displaying Loader
                                    progressDialog.dismiss();

                                    Toast.makeText(WishMatch.this, getResources()
                                            .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } // Catch inside onResponse ends here
                    } // if condition on request body ends here
                } // onResponse ends here
            }); // CallBack Ends here
        }
        catch (Exception ex)
        {
            Log.e("SplashActivity", "ex", ex);
        }
    }
}
