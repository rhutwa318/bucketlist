package com.example.hardikdesaii.bucketlist.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.hardikdesaii.bucketlist.Fragments.EditProfileFragment;
import com.example.hardikdesaii.bucketlist.Fragments.EnterWishFragment;
import com.example.hardikdesaii.bucketlist.Fragments.HomeFragment;
import com.example.hardikdesaii.bucketlist.Fragments.MyWishFragment;
import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Home extends AppCompatActivity {
    private Typeface webFont;
    private TextView home, wish, list, edit;
    private FragmentManager fManager;
    FragmentTransaction fTransaction;
    private TextView title;
    private TextView logout;
    public static boolean mLoading = false;
    private static String currentWish = "null";
    public static String mEmail;
    private MaterialDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupUI();
        mEmail = SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_EMAIL);
        fManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            fTransaction = fManager.beginTransaction();
            fTransaction.add(R.id.fl_fragments, HomeFragment.newInstance(), "");

            fTransaction.commit();
            home.setTextColor(Color.parseColor("#000000"));
            wish.setTextColor(Color.parseColor("#FFFFFF"));
            list.setTextColor(Color.parseColor("#FFFFFF"));
            edit.setTextColor(Color.parseColor("#FFFFFF"));

        }
    }

    public static String getCurrentWish() {
        return currentWish;
    }

    public static void setCurrentWish(String currentWish) {
        Home.currentWish = currentWish;
    }



    public void onSelectWish() {
        title.setText("SELECT WISH");
        fTransaction = fManager.beginTransaction();
        fTransaction.replace(R.id.fl_fragments, EnterWishFragment.newInstance(), "");
        fTransaction.commit();
        wish.setTextColor(Color.parseColor("#000000"));
        list.setTextColor(Color.parseColor("#FFFFFF"));
        edit.setTextColor(Color.parseColor("#FFFFFF"));
        home.setTextColor(Color.parseColor("#FFFFFF"));
    }





    private void setupUI() {

        webFont = Typeface.createFromAsset(Home.this.getAssets(), "fonts/fontawesome-webfont.ttf");
        home = (TextView) findViewById(R.id.tv_home);
        wish = (TextView) findViewById(R.id.tv_enterWish);
        list = (TextView) findViewById(R.id.tv_myWishList);
        edit = (TextView) findViewById(R.id.tv_editProfile);
        title = (TextView) findViewById(R.id.tv_titleHome);
        logout = (TextView) findViewById(R.id.tv_logout);
        logout.setTypeface(webFont);
        logout.setClickable(true);

        home.setTypeface(webFont);
        wish.setTypeface(webFont);
        list.setTypeface(webFont);
        edit.setTypeface(webFont);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(Home.this, Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        // set onClick listeners for all components
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("EDIT PROFILE");
                fTransaction = fManager.beginTransaction();
                fTransaction.replace(R.id.fl_fragments, EditProfileFragment.newInstance(), "");
                fTransaction.commit();
                edit.setTextColor(Color.parseColor("#000000"));
                home.setTextColor(Color.parseColor("#FFFFFF"));
                wish.setTextColor(Color.parseColor("#FFFFFF"));
                list.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("HOME");

                fTransaction = fManager.beginTransaction();
                fTransaction.replace(R.id.fl_fragments, HomeFragment.newInstance(), "");
                fTransaction.commit();
                home.setTextColor(Color.parseColor("#000000"));
                wish.setTextColor(Color.parseColor("#FFFFFF"));
                list.setTextColor(Color.parseColor("#FFFFFF"));
                edit.setTextColor(Color.parseColor("#FFFFFF"));

            }
        });
        wish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("SELECT WISH");
                fTransaction = fManager.beginTransaction();
                fTransaction.replace(R.id.fl_fragments, EnterWishFragment.newInstance(), "");
                fTransaction.commit();
                wish.setTextColor(Color.parseColor("#000000"));
                list.setTextColor(Color.parseColor("#FFFFFF"));
                edit.setTextColor(Color.parseColor("#FFFFFF"));
                home.setTextColor(Color.parseColor("#FFFFFF"));

            }
        });
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText("MY WISH LIST");

                fTransaction = fManager.beginTransaction();
                fTransaction.replace(R.id.fl_fragments, MyWishFragment.newInstance(), "");
                fTransaction.commit();
                list.setTextColor(Color.parseColor("#000000"));
                edit.setTextColor(Color.parseColor("#FFFFFF"));
                home.setTextColor(Color.parseColor("#FFFFFF"));
                wish.setTextColor(Color.parseColor("#FFFFFF"));


            }
        });

    }

    public void deleteWish(final String wishName, final ArrayList<String> wishList) {
        // Start the ProgressDailog
        progressDialog = new MaterialDialog.Builder(Home.this)
                .content("Please wait...")
                .progress(true, 0)
                .show();
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        mLoading = true;
        supportInvalidateOptionsMenu();
        RequestBody requestBody = new FormBody.Builder()
                .add(BucketListConstants.CUSTOMER_EMAIL, mEmail)
                .add(BucketListConstants.WISH, wishName)
                .build();

        try {
            final Request request = new Request.Builder()
                    .url("https://poojathakor99.000webhostapp.com/BucketList/android_wish_delete.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, final IOException e) {
                    Log.e("productFragement", "onFailure", e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // stop showing progress Bar
                            progressDialog.dismiss();
                            Toast.makeText(Home.this,
                                    getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                        }
                    });
                } // onFailure ends here

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("response", " " + response);
                    Log.e("response.body", " " + response.body());

                    if (response.body() != null) {
                        final String result = response.body().string();
                        try {
                            final JSONObject jsonObj = new JSONObject(result);
                            Log.e("Inside onResponse Try", result);
                            final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                            final boolean auth = jsonObj.getBoolean(BucketListConstants.AUTH);

                            Log.e("Mstatus ", "" + mMessage);

                            // if mStatus matches the server status
                            if (auth == true) {
                                // show the message in either case
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run()
                                    {
                                        wishList.remove(wishName);
                                        // stop displaying Loader
                                        progressDialog.dismiss();
                                        // Take user to Menu activity
                                        Toast.makeText(Home.this, "Wish deleted Successfully", Toast.LENGTH_SHORT).show();

                                    }
                                });
                            } else {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // stop displaying Loader
                                        progressDialog.dismiss();

                                        Toast.makeText(Home.this, mMessage, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        } // try insde onResponse ends here
                        catch (final JSONException e) {
                            Log.e("productFragement", "Inside on Response try", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // stop displaying Loader
                                    progressDialog.dismiss();

                                    Toast.makeText(Home.this, getResources()
                                            .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } // Catch inside onResponse ends here
                    } // if condition on request body ends here
                } // onResponse ends here
            }); // CallBack Ends here
        } catch (Exception ex) {
            Log.e("SplashActivity", "ex", ex);

        }


    }
}
