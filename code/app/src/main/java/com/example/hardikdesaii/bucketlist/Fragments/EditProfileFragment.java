package com.example.hardikdesaii.bucketlist.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.hardikdesaii.bucketlist.Activities.Home;
import com.example.hardikdesaii.bucketlist.Activities.Signup;
import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.DetectConnection;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class EditProfileFragment extends Fragment implements View.OnClickListener
{
    TextInputLayout mlayoutlastname, mlayoutfirstname, mlayoutemail, mlayoutpassword, mlayoutcpassword;
    EditText mFirstName, mLastName, muserEmail, muserPassword, mconfirmPassword;
    Toolbar toolbarSingup;
    TextView maddAccount, mcheckShowPassword, mcheckShowConfirmPassword, tv_Signup_signinLink;
    Boolean isPasswordVisible = true;
    private Button btnsignin;
    private RadioGroup radioSexGroup;
    private String gender;
    private View view;
    private RadioButton radioSexButton;
    private String fname;
    private String lname;
    private String email;
    private String password;
    private String fcmkey;
    private MaterialDialog progressDialog;



    public EditProfileFragment() {
    }

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        fname= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_FIRSTNAME);
        lname= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_LASTNAME);
        email= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_EMAIL);
        password= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.CUSTOMER_PASSWORD);
        fcmkey= SharedPrefHelper.getPrefsHelper().getPref(SharedPrefHelper.FCM_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setupUI(view);
        return view;
    }

    private void setupUI(View view)
    {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Raleway-Regular.ttf");
        mlayoutlastname = (TextInputLayout)view.findViewById(R.id.input_edit_last_name);
        mlayoutfirstname = (TextInputLayout)view. findViewById(R.id.input_edit_name);
        mlayoutemail = (TextInputLayout)view. findViewById(R.id.input_edit_email);
        mlayoutpassword = (TextInputLayout) view.findViewById(R.id.input_edit_password);
        mlayoutcpassword = (TextInputLayout)view. findViewById(R.id.input_edit_confirm_pass);

        mFirstName = (EditText) view.findViewById(R.id.et_edit_name);
        mFirstName.setTypeface(tf);
        mFirstName.setText(fname);
        mLastName = (EditText) view.findViewById(R.id.et_edit_lastname);
        mLastName.setTypeface(tf);
        mLastName.setText(lname);
        muserEmail = (EditText) view.findViewById(R.id.et_edit_email);
        muserEmail.setTypeface(tf);
        muserEmail.setText(email);

        muserPassword = (EditText) view.findViewById(R.id.edit_password);
        muserPassword.setTypeface(tf);
        mconfirmPassword = (EditText)view. findViewById(R.id.edit_confirm_password);
        mconfirmPassword.setTypeface(tf);

        mcheckShowPassword = (TextView)view. findViewById(R.id.tv_edit_show_password);
        mcheckShowPassword.setOnClickListener(this);

        mcheckShowConfirmPassword = (TextView)view. findViewById(R.id.tv_edit_hide_show_confirmpassword);
        mcheckShowConfirmPassword.setOnClickListener(this);

        btnsignin = (Button)view. findViewById(R.id.btn_edit_submit);
        btnsignin.setOnClickListener(this);
        radioSexGroup=(RadioGroup)view.findViewById(R.id.rg_gender);
        setFontsForText();

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.btn_edit_submit:
                if (signUpValidate())
                {
                    updateService();
                }
                break;
            case R.id.tv_edit_show_password:
                userhideshowpassword();
                break;

            case R.id.tv_edit_hide_show_confirmpassword:
                userhideshowconfirmpassword();
                break;


        }

    }

    private void updateService()
    {
        String firstName=mFirstName.getText().toString();
        String lastName=mLastName.getText().toString();
        String memail=muserEmail.getText().toString();
        String password=mconfirmPassword.getText().toString();
        if(DetectConnection.checkInternetConnection(getActivity()))
        {
            progressDialog = new MaterialDialog.Builder(getActivity())
                    .content("Please wait...")
                    .progress(true, 0)
                    .show();

            // Start the ProgressDailog
            final OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
            RequestBody requestBody = new FormBody.Builder()
                    .add("fname", firstName)
                    .add("lname", lastName)
                    .add("old_email", email)
                    .add("pwd", password)
                    .build();

            try {
                final Request request = new Request.Builder()
                        .url("https://poojathakor99.000webhostapp.com/BucketList/android_editprofile.php")
                        .post(requestBody)
                        .build();

                client.newCall(request).enqueue(new Callback() {

                    @Override
                    public void onFailure(Call call, final IOException e) {
                        Log.e("productFragement", "onFailure", e);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                // stop showing progress Bar
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(),
                                        getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                            }
                        });
                    } // onFailure ends here

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.e("response", " " + response);
                        Log.e("response.body", " " + response.body());

                        if (response.body() != null) {
                            final String result = response.body().string();
                            try {
                                final JSONObject jsonObj = new JSONObject(result);
                                Log.e("Inside onResponse Try", result);
                                final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                                final boolean auth = jsonObj.getBoolean(BucketListConstants.AUTH);

                                Log.e("Mstatus ", "" + mMessage);

                                // if mStatus matches the server status
                                if (auth == true) {
                                    // show the message in either case
                                    getActivity().runOnUiThread(new Runnable() {

                                        @Override
                                        public void run()
                                        {
                                            // stop displaying Loader
                                            progressDialog.dismiss();
                                            // Take user to Menu activity
                                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                } else {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            // stop displaying Loader
                                            progressDialog.dismiss();
                                            Toast.makeText(getActivity(), mMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }


                            } // try insde onResponse ends here
                            catch (final JSONException e) {
                                Log.e("productFragement", "Inside on Response try", e);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // stop displaying Loader
                                        progressDialog.dismiss();
                                        Toast.makeText(getActivity(), getResources()
                                                .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } // Catch inside onResponse ends here
                        } // if condition on request body ends here
                    } // onResponse ends here
                }); // CallBack Ends here
            } catch (Exception ex) {
                Log.e("SplashActivity", "ex", ex);

            }

        }
        else
        {
            Toast.makeText(getActivity(),"Check internet connection !",Toast.LENGTH_SHORT).show();
        }


    }


    private void setFontsForText() {

        /**
         * Changing the font face, font size, font color, action bar title
         * using the TypeFace method of android
         */
        String fontRalewayBold = "fonts/Raleway-Bold.ttf";
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontRalewayBold);
        //titleAccount.setTypeface(tf);
        mcheckShowPassword.setTypeface(tf);
        mcheckShowConfirmPassword.setTypeface(tf);


        String fontRalewayLight = "fonts/Raleway-Regular.ttf";
        Typeface type = Typeface.createFromAsset(getActivity().getAssets(), fontRalewayLight);

        muserPassword.setTypeface(type);
        mFirstName.setTypeface(type);
        mLastName.setTypeface(type);
        muserEmail.setTypeface(type);
        muserPassword.setTypeface(type);
        mconfirmPassword.setTypeface(type);

    }
    private void setButtonEnable(boolean isEnable)
    {
        if (!isEnable) {
            mFirstName.setEnabled(false);
            mLastName.setEnabled(false);
            muserEmail.setEnabled(false);
            muserPassword.setEnabled(false);
            mconfirmPassword.setEnabled(false);
            btnsignin.setEnabled(false);
        } else {
            mFirstName.setEnabled(true);
            mLastName.setEnabled(true);
            muserEmail.setEnabled(true);
            muserPassword.setEnabled(true);
            mconfirmPassword.setEnabled(true);
            btnsignin.setEnabled(true);
        }
    }

    private boolean validEmail(String email) {
        String email_pattern = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(email_pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private void userhideshowpassword() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mcheckShowPassword.setText(R.string.tv_hide_password);
            muserPassword.setTransformationMethod(null);
            muserPassword.setSelection(muserPassword.getText().length());
            mcheckShowPassword.setVisibility(View.GONE);
            mcheckShowPassword.setVisibility(View.VISIBLE);

        } else {

            // Hide password
            isPasswordVisible = true;
            muserPassword.setTransformationMethod(new PasswordTransformationMethod());
            muserPassword.setSelection(muserPassword.getText().length());
            mcheckShowPassword.setText(R.string.tv_show_password);
            mcheckShowPassword.setVisibility(View.GONE);
            mcheckShowPassword.setVisibility(View.VISIBLE);
        }
    }

    /**
     * userhideshowconfirmpassword() method is used to password field in show and hide password perform
     */
    private void userhideshowconfirmpassword() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mcheckShowConfirmPassword.setText(R.string.tv_hide_password);
            mconfirmPassword.setTransformationMethod(null);
            mconfirmPassword.setSelection(mconfirmPassword.getText().length());
            mcheckShowConfirmPassword.setVisibility(View.GONE);
            mcheckShowConfirmPassword.setVisibility(View.VISIBLE);

        } else {

            // Hide password
            isPasswordVisible = true;
            mcheckShowConfirmPassword.setText(R.string.tv_show_password);
            mconfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
            mconfirmPassword.setSelection(mconfirmPassword.getText().length());
            mcheckShowConfirmPassword.setVisibility(View.GONE);
            mcheckShowConfirmPassword.setVisibility(View.VISIBLE);
        }
    }
    public void setUpComponents(boolean flag) {
        btnsignin.setClickable(flag);
        maddAccount.setClickable(flag);
        tv_Signup_signinLink.setClickable(flag);

    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private Boolean signUpValidate() {
        /**
         * A flag is initialized for checking Edittext value is empty or not
         */
        Boolean logInCheck = true;
        final String Firstname = mFirstName.getText().toString().trim();
        final String Lastname = mLastName.getText().toString().trim();
        final String UserName = muserEmail.getText().toString().trim();
        final String pass = muserPassword.getText().toString().trim();

        Log.e("Firstname", Firstname);
        Log.e("Lastname", Lastname);
        Log.e("Email", UserName);
        Log.e("Password", pass);

        /**
         *  validation: edittext is empty
         */
        if (Firstname.equals("")) {
            mlayoutfirstname.setError(this.getResources().getString(R.string.enterFirstErrMsg));
            requestFocus(mFirstName);
            logInCheck = false;
        } else
        {
            mlayoutfirstname.setError(null);
            requestFocus(mFirstName);
        }
        if (Lastname.equals("")) {
            mlayoutlastname.setError(this.getResources().getString(R.string.enterLastErrMsg));
            requestFocus(mLastName);
            logInCheck = false;
        } else {
            mlayoutlastname.setError(null);
            requestFocus(mLastName);
        }
        if(!(pass.equals(password)))
        {
            mlayoutpassword.setError("Old password incorrect !");
            requestFocus(muserPassword);
            logInCheck = false;
        } else {
            mlayoutpassword.setError(null);
            requestFocus(muserPassword);
        }
        if (UserName.isEmpty() || !validEmail(UserName)) {
            mlayoutemail.setError(getString(R.string.enterSignIdErrMsg));
            requestFocus(muserEmail);
            logInCheck = false;
        } else {
            mlayoutemail.setError(null);
            requestFocus(muserEmail);
        }
        int selectedId=radioSexGroup.getCheckedRadioButtonId();
        try
        {
            radioSexButton=(RadioButton)view.findViewById(selectedId);
            gender=radioSexButton.getText().toString();
        }
        catch (Exception ex)
        {
            gender=null;
        }
        if((gender==null))
        {
            logInCheck=false;
        }


        return logInCheck;
    }
}
