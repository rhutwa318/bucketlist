package com.example.hardikdesaii.bucketlist.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hardikdesaii.bucketlist.Adapters.HomeListAdapter;
import com.example.hardikdesaii.bucketlist.Adapters.SelectWishAdapter;
import com.example.hardikdesaii.bucketlist.Entities.HomeListData;
import com.example.hardikdesaii.bucketlist.R;

import java.util.ArrayList;


public class HomeFragment extends Fragment
{
    private RecyclerView recyclerView;
    private HomeListAdapter adapter;
    private ArrayList<HomeListData> list;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        list=new ArrayList<>();
        addValues();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_homeFragment);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter=new HomeListAdapter(getActivity(),list);
        recyclerView.setAdapter(adapter);

        return view;
    }

    public void addValues()
    {
        list.add(new HomeListData("Formals For Her",R.drawable.img_one));
        list.add(new HomeListData("Casual  For Her",R.drawable.img_two));
        list.add(new HomeListData("Kids Girls",R.drawable.img_three));
        list.add(new HomeListData("Kids Boy",R.drawable.img_four));
        list.add(new HomeListData("Men's Blazers",R.drawable.img_five));
        list.add(new HomeListData("Men's Casuals",R.drawable.img_six));

    }



}
