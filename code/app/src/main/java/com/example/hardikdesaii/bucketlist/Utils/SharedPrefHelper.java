package com.example.hardikdesaii.bucketlist.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by HardikDesaii on 07/05/17.
 */

public class SharedPrefHelper
{
    // Sharedpref file name
    public static final String PREF_NAME = "BucketList";
    // customer email
    public static final String CUSTOMER_FIRSTNAME = "firstname";

    // customer email
    public static final String CUSTOMER_LASTNAME = "lastname";

    // customer email
    public static final String CUSTOMER_PASSWORD = "password";

    // customer email
    public static final String CUSTOMER_BDATE = "bday";

    // customer email
    public static final String CUSTOMER_EMAIL = "email";

    // to know if user is logged in or not
    public static final String PREF_ISLOGIN = "is_login";

    public static final String FCM_KEY="fcm_key";


    // for storing the stepindicator steps for current category
    private static SharedPrefHelper instance;
    // Shared Preferences
    private SharedPreferences sharedPreferences;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    /**
     * SharedPrefHelper constructor set in Myapplication class
     *
     * @param context
     */
    public SharedPrefHelper(Context context) {
        instance = this;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPrefHelper getPrefsHelper() {
        return instance;
    }

    public void deleteAll() {
        editor.clear().apply();

    }

    public void delete(String key) {
        if (sharedPreferences.contains(key)) {
            editor.remove(key).commit();
        }
    }

    public void setData(String key, Object value) {

        editor = sharedPreferences.edit();
        delete(key);
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-primitive preference");
        }

        editor.commit();
    }

    @SuppressWarnings("unchecked")
    public <T> T getPref(String key) {
        return (T) sharedPreferences.getAll().get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T getPref(String key, T defValue) {
        T returnValue = (T) sharedPreferences.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public boolean isPrefExists(String key) {
        return sharedPreferences.contains(key);
    }
}
