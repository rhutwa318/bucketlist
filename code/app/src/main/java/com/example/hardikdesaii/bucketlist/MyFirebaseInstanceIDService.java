package com.example.hardikdesaii.bucketlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Hp on 13-05-2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    String token;
    final String Myprefrences="prefrence";
    @Override
    public void onTokenRefresh()
    {
        super.onTokenRefresh();
        token= FirebaseInstanceId.getInstance().getToken();
        //SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.FCM_KEY,token);
        //Toast.makeText(this,"Thanks",Toast.LENGTH_LONG).show();
        Log.d("hardik","Refreshed token:" + token);
        SharedPreferences sharedprefrence = getSharedPreferences(Myprefrences, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor =sharedprefrence.edit();
        editor.putString("token",token);
        editor.commit();
        registerToken(token);

    }

    private void registerToken(String token)
    {
        SharedPreferences sharedprefrence = getSharedPreferences(Myprefrences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =sharedprefrence.edit();
        editor.putString("token",token);
        editor.commit();
    }

}
