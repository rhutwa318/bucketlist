package com.example.hardikdesaii.bucketlist.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.DetectConnection;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Signup extends AppCompatActivity implements View.OnClickListener
{
    TextInputLayout mlayoutlastname, mlayoutfirstname, mlayoutemail, mlayoutpassword, mlayoutcpassword;
    EditText mFirstName, mLastName, muserEmail, muserPassword, mconfirmPassword;
    Toolbar toolbarSingup;
    TextView maddAccount, mcheckShowPassword, mcheckShowConfirmPassword, tv_Signup_signinLink;
    Boolean isPasswordVisible = true;
    private boolean mLoading = false;
    private Button btnsignin;
    private Calendar calendar;
    private int year, month, day;
    private TextView tv_Bdate;
    private String date="";
    private String fcm_key;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        //set Toolbar
        setToolbarData();
        //set up all UI componnent
        setUIComponent();
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        //FirebaseMessaging.getInstance().subscribeToTopic("test");
        //fcm_key=FirebaseInstanceId.getInstance().getToken();
        //Toast.makeText(Signup.this,""+fcm_key,Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem progressMenuItem = menu.findItem(R.id.action_submit_signup_progressbar);


        if (this.mLoading) {

            progressMenuItem.setVisible(true);
            MenuItemCompat.setActionView(progressMenuItem, R.layout.item_menu_progressbar_layout);

        } else {
            progressMenuItem.setVisible(false);
            MenuItemCompat.setActionView(progressMenuItem, null);
        }
        return super.onPrepareOptionsMenu(menu);
    }
    /**
     * set Toolbar name
     */
    private void setToolbarData() {
        toolbarSingup = (Toolbar) findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbarSingup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarSingup.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();            }
        });
    }
    /**
     * initialization all component
     */
    private void setUIComponent()
    {
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Regular.ttf");
        mlayoutlastname = (TextInputLayout) findViewById(R.id.input_layout_last_name);
        mlayoutfirstname = (TextInputLayout) findViewById(R.id.input_layout_name);
        mlayoutemail = (TextInputLayout) findViewById(R.id.input_layout_email);
        mlayoutpassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        mlayoutcpassword = (TextInputLayout) findViewById(R.id.input_layout_confirm_pass);

        mFirstName = (EditText) findViewById(R.id.et_Signup_name);
        mFirstName.setTypeface(tf);
        mLastName = (EditText) findViewById(R.id.et_Signup_lastname);
        mLastName.setTypeface(tf);
        muserEmail = (EditText) findViewById(R.id.et_Signup_email);
        muserEmail.setTypeface(tf);
        muserPassword = (EditText) findViewById(R.id.Signup_password);
        muserPassword.setTypeface(tf);
        mconfirmPassword = (EditText) findViewById(R.id.Signup_confirm_password);
        mconfirmPassword.setTypeface(tf);

        mcheckShowPassword = (TextView) findViewById(R.id.tv_Signup_show_password);
        mcheckShowPassword.setOnClickListener(this);

        mcheckShowConfirmPassword = (TextView) findViewById(R.id.tv_Signup_hide_show_confirmpassword);
        mcheckShowConfirmPassword.setOnClickListener(this);

        btnsignin = (Button) findViewById(R.id.btn_Signup_submit);
        btnsignin.setOnClickListener(this);

        maddAccount = (TextView) findViewById(R.id.tv_sign_up_link);
        maddAccount.setOnClickListener(this);
        tv_Bdate=(TextView)findViewById(R.id.tv_Bdate);
        tv_Bdate.setOnClickListener(this);
        date=tv_Bdate.getText().toString();

        tv_Signup_signinLink = (TextView) findViewById(R.id.tv_Signup_signinLink);
        tv_Signup_signinLink.setOnClickListener(this);
        setFontsForText();

    }
    private void setButtonEnable(boolean isEnable) {
        if (!isEnable) {
            mFirstName.setEnabled(false);
            mLastName.setEnabled(false);
            muserEmail.setEnabled(false);
            muserPassword.setEnabled(false);
            mconfirmPassword.setEnabled(false);
            btnsignin.setEnabled(false);
        } else {
            mFirstName.setEnabled(true);
            mLastName.setEnabled(true);
            muserEmail.setEnabled(true);
            muserPassword.setEnabled(true);
            mconfirmPassword.setEnabled(true);
            btnsignin.setEnabled(true);
        }
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_Signup_submit:
               // Toast.makeText(Signup.this,"click",Toast.LENGTH_SHORT).show();
                    if (signUpValidate())
                    {
                        if(DetectConnection.checkInternetConnection(Signup.this))
                        {
                            FirebaseMessaging.getInstance().subscribeToTopic("test");
                            fcm_key=FirebaseInstanceId.getInstance().getToken();
                            Toast.makeText(Signup.this,""+fcm_key,Toast.LENGTH_SHORT).show();
                            submitForm();
                        }
                        else
                        {
                            Toast.makeText(Signup.this,"No Internet connection !",Toast.LENGTH_SHORT).show();
                        }

                    }

                break;
            case R.id.tv_Signup_show_password:
                userhideshowpassword();
                break;

            case R.id.tv_Signup_hide_show_confirmpassword:
                userhideshowconfirmpassword();
                break;

            case R.id.tv_sign_up_link:
            case R.id.tv_Signup_signinLink:
                Signup.this.finish();
                break;
            case  R.id.tv_Bdate:
                showDialog(999);
                break;
        }
    }

    private void submitForm()
    {
        final String mFirstname = mFirstName.getText().toString();
        final String mLastname = mLastName.getText().toString();
        final String mEmail = muserEmail.getText().toString().trim();
        final String mPassword = muserPassword.getText().toString();
        // Start the ProgressDailog
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        mLoading = true;
        supportInvalidateOptionsMenu();
        setUpComponents(false);
        setButtonEnable(false);
        RequestBody requestBody = new FormBody.Builder()
                .add(BucketListConstants.USER_FIRSTNAME, mFirstname)
                .add(BucketListConstants.USER_LASTNAME, mLastname)
                .add(BucketListConstants.CUSTOMER_EMAIL, mEmail)
                .add(BucketListConstants.CUSTOMER_PASSWORD, mPassword)
                .add(BucketListConstants.BIRTH_DATE,date)
                .add(BucketListConstants.FCM_KEY,fcm_key)
                .build();

        try {
            final Request request = new Request.Builder()
                    .url("https://poojathakor99.000webhostapp.com/BucketList/android_signup.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, final IOException e) {
                    Log.e("productFragement", "onFailure", e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // stop showing progress Bar

                            mLoading = false;
                            supportInvalidateOptionsMenu();
                            setUpComponents(true);
                            setButtonEnable(true);
                            Toast.makeText(Signup.this,
                                    getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                        }
                    });
                } // onFailure ends here

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("response", " " + response);
                    Log.e("response.body", " " + response.body());

                    if (response.body() != null) {
                        final String result = response.body().string();
                        try {
                            final JSONObject jsonObj = new JSONObject(result);
                            Log.e("Inside onResponse Try", result);
                            final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                            final boolean auth=jsonObj.getBoolean(BucketListConstants.AUTH);
                            Log.e("Mstatus ", "" + mMessage);

                            // if mStatus matches the server status
                            if (auth==true)
                            {

                                // if successfully logged in , then store key,customer_id and email in SharedPrefrence
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_EMAIL,mEmail );
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_FIRSTNAME, mFirstname);
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_LASTNAME, mLastname);
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_BDATE, date);
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_PASSWORD, mPassword);
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.FCM_KEY, fcm_key);
                                SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.PREF_ISLOGIN, true);

                                // show the message in either case
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Signup.this, mMessage, Toast.LENGTH_SHORT).show();
                                        Intent intent=new Intent(Signup.this,Home.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        // Take user to Menu activity

                                    }
                                });
                            } else
                                {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Signup.this, mMessage, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        } // try insde onResponse ends here
                        catch (final JSONException e) {
                            Log.e("productFragement", "Inside on Response try", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // stop displaying Loader
                                    mLoading = false;
                                    supportInvalidateOptionsMenu();
                                    setUpComponents(true);
                                    setButtonEnable(true);
                                    Toast.makeText(Signup.this, getResources()
                                            .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } // Catch inside onResponse ends here
                    } // if condition on request body ends here
                } // onResponse ends here
            }); // CallBack Ends here
        }
        catch (Exception ex)
        {
            Log.e("SplashActivity", "ex", ex);
        }
    } // submitForm ends here..


    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999)
        {

            return new DatePickerDialog(Signup.this,myDateListener, year, month, day);
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    showDate(arg1, arg2 + 1, arg3);
                }
            };
    private void showDate(int year, int month, int day)
    {
        String localDate=day+"-"+month+"-"+year;
        date=localDate;
        tv_Bdate.setText(localDate);
    }


    private boolean passwordMatch() {
        String pass = muserPassword.getText().toString();
        String cpass = mconfirmPassword.getText().toString();
        if (!pass.equals(cpass)) {
            mlayoutcpassword.setError(getString(R.string.err_msg_passmatch));
            requestFocus(mconfirmPassword);
            return false;
        } else {
            mlayoutcpassword.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validEmail(String email) {
        String email_pattern = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(email_pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * userhideshowpassword() method is used to password field in show and hide password perform
     */

    private void userhideshowpassword() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mcheckShowPassword.setText(R.string.tv_hide_password);
            muserPassword.setTransformationMethod(null);
            muserPassword.setSelection(muserPassword.getText().length());
            mcheckShowPassword.setVisibility(View.GONE);
            mcheckShowPassword.setVisibility(View.VISIBLE);

        } else {

            // Hide password
            isPasswordVisible = true;
            muserPassword.setTransformationMethod(new PasswordTransformationMethod());
            muserPassword.setSelection(muserPassword.getText().length());
            mcheckShowPassword.setText(R.string.tv_show_password);
            mcheckShowPassword.setVisibility(View.GONE);
            mcheckShowPassword.setVisibility(View.VISIBLE);
        }
    }

    /**
     * userhideshowconfirmpassword() method is used to password field in show and hide password perform
     */
    private void userhideshowconfirmpassword() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            mcheckShowConfirmPassword.setText(R.string.tv_hide_password);
            mconfirmPassword.setTransformationMethod(null);
            mconfirmPassword.setSelection(mconfirmPassword.getText().length());
            mcheckShowConfirmPassword.setVisibility(View.GONE);
            mcheckShowConfirmPassword.setVisibility(View.VISIBLE);

        } else {

            // Hide password
            isPasswordVisible = true;
            mcheckShowConfirmPassword.setText(R.string.tv_show_password);
            mconfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
            mconfirmPassword.setSelection(mconfirmPassword.getText().length());
            mcheckShowConfirmPassword.setVisibility(View.GONE);
            mcheckShowConfirmPassword.setVisibility(View.VISIBLE);
        }
    }
    private void setFontsForText() {

        /**
         * Changing the font face, font size, font color, action bar title
         * using the TypeFace method of android
         */
        String fontRalewayBold = "fonts/Raleway-Bold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontRalewayBold);
        //titleAccount.setTypeface(tf);
        mcheckShowPassword.setTypeface(tf);
        mcheckShowConfirmPassword.setTypeface(tf);


        String fontRalewayLight = "fonts/Raleway-Regular.ttf";
        Typeface type = Typeface.createFromAsset(getAssets(), fontRalewayLight);

        muserPassword.setTypeface(type);
        mFirstName.setTypeface(type);
        mLastName.setTypeface(type);
        muserEmail.setTypeface(type);
        muserPassword.setTypeface(type);
        mconfirmPassword.setTypeface(type);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setUpComponents(boolean flag) {
        btnsignin.setClickable(flag);
        maddAccount.setClickable(flag);
        tv_Signup_signinLink.setClickable(flag);

    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private Boolean signUpValidate() {
        /**
         * A flag is initialized for checking Edittext value is empty or not
         */
        Boolean logInCheck = true;
        final String Firstname = mFirstName.getText().toString().trim();
        final String Lastname = mLastName.getText().toString().trim();
        final String UserName = muserEmail.getText().toString().trim();
        final String pass = muserPassword.getText().toString().trim();

        Log.e("Firstname", Firstname);
        Log.e("Lastname", Lastname);
        Log.e("Email", UserName);
        Log.e("Password", pass);

        /**
         *  validation: edittext is empty
         */
        if (Firstname.equals("")) {
            mlayoutfirstname.setError(this.getResources().getString(R.string.enterFirstErrMsg));
            requestFocus(mFirstName);
            logInCheck = false;
        } else {
            mlayoutfirstname.setError(null);
            requestFocus(mFirstName);
        }
        if (Lastname.equals("")) {
            mlayoutlastname.setError(this.getResources().getString(R.string.enterLastErrMsg));
            requestFocus(mLastName);
            logInCheck = false;
        } else {
            mlayoutlastname.setError(null);
            requestFocus(mLastName);
        }
        if (pass.isEmpty() || !passwordMatch()) {
            mlayoutpassword.setError(this.getResources().getString(R.string.enterPasswordErrMsg));
            requestFocus(muserPassword);
            logInCheck = false;
        } else {
            mlayoutpassword.setError(null);
            requestFocus(muserPassword);
        }
        if (UserName.isEmpty() || !validEmail(UserName)) {
            mlayoutemail.setError(getString(R.string.enterSignIdErrMsg));
            requestFocus(muserEmail);
            logInCheck = false;
        } else {
            mlayoutemail.setError(null);
            requestFocus(muserEmail);
        }
        date=tv_Bdate.getText().toString();
        if(date.equals("Select Birth Date"))
        {
            logInCheck=false;
            Toast.makeText(Signup.this,"Please select your Birth Date",Toast.LENGTH_SHORT).show();
        }
        return logInCheck;
    }

} // main class ends here
