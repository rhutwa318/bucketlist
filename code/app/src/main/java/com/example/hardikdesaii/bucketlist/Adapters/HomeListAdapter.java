package com.example.hardikdesaii.bucketlist.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hardikdesaii.bucketlist.Activities.Home;
import com.example.hardikdesaii.bucketlist.Entities.HomeListData;
import com.example.hardikdesaii.bucketlist.R;

import java.util.ArrayList;

/**
 * Created by HardikDesaii on 10/05/17.
 */

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.CreateViewHolder>
{
    public Context context;
    public ArrayList<HomeListData> list;

    public HomeListAdapter(Context context, ArrayList<HomeListData> list)
    {
        this.context=context;
        this.list=list;
    }
    @Override
    public HomeListAdapter.CreateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_home_list, parent, false);
        return new HomeListAdapter.CreateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeListAdapter.CreateViewHolder holder, int position)
    {
        HomeListData item=list.get(position);
        Glide.with(context).load(item.getImage()).placeholder(R.drawable.bucketlist).centerCrop().into(holder.iv_bg);
        holder.title.setText(item.getTitle());
        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof Home)
                {
                    ((Home) context).onSelectWish();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CreateViewHolder extends RecyclerView.ViewHolder
    {
        ImageView iv_bg;
        TextView title;
        CardView cardview;
        public CreateViewHolder(View itemView)
        {
            super(itemView);
            String fontRalewayBold = "fonts/Raleway-Bold.ttf";
            Typeface tf = Typeface.createFromAsset(context.getAssets(), fontRalewayBold);

            iv_bg=(ImageView)itemView.findViewById(R.id.iv_mainHome);
            title=(TextView)itemView.findViewById(R.id.tv_homeList);
            cardview=(CardView)itemView.findViewById(R.id.cv_homeList);
            title.setTypeface(tf);
        }
    }
}
