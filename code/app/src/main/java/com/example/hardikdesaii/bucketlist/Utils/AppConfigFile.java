package com.example.hardikdesaii.bucketlist.Utils;

import android.app.Application;

/**
 * Created by HardikDesaii on 07/05/17.
 */

public class AppConfigFile extends Application
{
    @Override
    public void onCreate() {

        super.onCreate();

        new SharedPrefHelper(this);
    }
}
