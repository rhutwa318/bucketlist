package com.example.hardikdesaii.bucketlist.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.example.hardikdesaii.bucketlist.R;
import com.example.hardikdesaii.bucketlist.Utils.BucketListConstants;
import com.example.hardikdesaii.bucketlist.Utils.SharedPrefHelper;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity implements View.OnClickListener
{
    TextInputLayout mlayoutemail, mlayoutpassword, mTilForgotPassword;
    EditText muserEmailAddress, muserPassword;
    EditText userForgotPassword;
    TextView mforgotpasswor, msignup, mhideshowPassword, tv_login_infomsg;
    Boolean isPasswordVisible = true;
    private boolean mLoading = false;
    private Button muserLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       

        /**
         * setupToolbar method passed here
         */
        setToolbar();
        /**
         * used to setup UI components
         */
        setupUIData();
        //set font typeface
        setFontsForText();
    }
    /**
     * Inflating menu in the action bar to display cart icon and list icon
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem progressMenuItem = menu.findItem(R.id.action_submit_login_progressbar);


        if (this.mLoading) {

            progressMenuItem.setVisible(true);
            MenuItemCompat.setActionView(progressMenuItem, R.layout.item_menu_progressbar_layout);

        } else {
            progressMenuItem.setVisible(false);
            MenuItemCompat.setActionView(progressMenuItem, null);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Sets up the toolbar for the activity.
     */
    private void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_login);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



    }
    /**
     * used to setup UI components
     */
    private void setupUIData() {

        muserLogin = (Button) findViewById(R.id.btn_Login_submit);
        muserLogin.setOnClickListener(this);

        mforgotpasswor = (TextView) findViewById(R.id.tv_login_forgot_password);
        mforgotpasswor.setOnClickListener(this);

        msignup = (TextView) findViewById(R.id.tv_login_sign_up_link);
        msignup.setOnClickListener(this);

        mlayoutemail = (TextInputLayout) findViewById(R.id.input_layout_email);
        mlayoutpassword = (TextInputLayout) findViewById(R.id.input_layout_pass);

        muserEmailAddress = (EditText) findViewById(R.id.et_login_email);
        muserPassword = (EditText) findViewById(R.id.et_login_password);

        mhideshowPassword = (TextView) findViewById(R.id.tv_login_hide_show_password);
        mhideshowPassword.setOnClickListener(this);
        tv_login_infomsg = (TextView) findViewById(R.id.tv_login_infomsg);
    }
    private void setButtonEnable(boolean isEnable) {
        if (!isEnable) {
            muserEmailAddress.setEnabled(false);
            muserPassword.setEnabled(false);
            muserLogin.setEnabled(false);
        } else {
            muserEmailAddress.setEnabled(true);
            muserPassword.setEnabled(true);
            muserLogin.setEnabled(true);
        }

        // mlayoutforgotpassword = (TextInputLayout) findViewById(R.id.til_forgot_password);
        // userForgotPassword = (EditText) findViewById(R.id.et_forgot_pass_email);
        //mProgressBar = (ProgressBar) findViewById(R.id.pb_login_progressBar);


    }
    /**
     * @param view get the id of the component which is Clicked
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_Login_submit:
                if (signInValidate())
                {
                    submitForm();
                }
                break;
            case R.id.tv_login_forgot_password:
                forgotDialog();
                break;
            case R.id.tv_login_sign_up_link:
            case R.id.tv_login_infomsg:
                Intent i = new Intent(Login.this, Signup.class);
                startActivity(i);
                break;
            case R.id.tv_login_hide_show_password:
                showHidePassword();
                break;


        }
    }

    private void submitForm()
    {
        final String mEmail = muserEmailAddress.getText().toString();
        final String mPassword = muserPassword.getText().toString();
        // Start the ProgressDailog
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        mLoading = true;
        supportInvalidateOptionsMenu();
        setUpComponents(false);
        setButtonEnable(false);
        RequestBody requestBody = new FormBody.Builder()
                .add(BucketListConstants.CUSTOMER_EMAIL, mEmail)
                .add(BucketListConstants.CUSTOMER_PASSWORD, mPassword)
                .build();

        try {
            final Request request = new Request.Builder()
                    .url("https://poojathakor99.000webhostapp.com/BucketList/android_login.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, final IOException e) {
                    Log.e("productFragement", "onFailure", e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // stop showing progress Bar

                            mLoading = false;
                            supportInvalidateOptionsMenu();
                            setUpComponents(true);
                            setButtonEnable(true);
                            Toast.makeText(Login.this,
                                    getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                        }
                    });
                } // onFailure ends here

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("response", " " + response);
                    Log.e("response.body", " " + response.body());

                    if (response.body() != null) {
                        final String result = response.body().string();
                        try {
                            final JSONObject jsonObj = new JSONObject(result);
                            Log.e("Inside onResponse Try", result);
                            final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                            final boolean auth=jsonObj.getBoolean(BucketListConstants.AUTH);

                            Log.e("Mstatus ", "" + mMessage);

                            // if mStatus matches the server status
                            if (auth==true)
                            {
                                final String fname=jsonObj.getString("fname");
                                final String lname=jsonObj.getString("lname");

                                // show the message in either case
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Login.this, mMessage, Toast.LENGTH_SHORT).show();
                                        SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_FIRSTNAME,fname);
                                        SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_LASTNAME,lname);
                                        SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_EMAIL,mEmail);
                                        SharedPrefHelper.getPrefsHelper().setData(SharedPrefHelper.CUSTOMER_PASSWORD,mPassword);
                                        Intent intent=new Intent(Login.this,Home.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        // Take user to Menu activity

                                    }
                                });
                            } else
                            {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Login.this, mMessage, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        } // try insde onResponse ends here
                        catch (final JSONException e) {
                            Log.e("productFragement", "Inside on Response try", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // stop displaying Loader
                                    mLoading = false;
                                    supportInvalidateOptionsMenu();
                                    setUpComponents(true);
                                    setButtonEnable(true);
                                    Toast.makeText(Login.this, getResources()
                                            .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } // Catch inside onResponse ends here
                    } // if condition on request body ends here
                } // onResponse ends here
            }); // CallBack Ends here
        }
        catch (Exception ex)
        {
            Log.e("SplashActivity", "ex", ex);
        }
    } // submit form ends here.

    /**
     * used to validate data which is inserted.
     */
    private Boolean signInValidate() {
        /**
         * A flag is initialized for checking Edittext value is empty or not
         */
        Boolean logInCheck = true;
        final String UserName = muserEmailAddress.getText().toString().trim();
        final String pass = muserPassword.getText().toString().trim();

        Log.e("Email", UserName);
        Log.e("Password", pass);

        /**
         *  validation: edittext is empty
         *
         */
        if (UserName.isEmpty() || !validEmail(UserName)) {
            mlayoutemail.setError(getString(R.string.enterSignIdErrMsg));
            requestFocus(muserEmailAddress);
            logInCheck = false;

        } else {
            mlayoutemail.setError(null);
            requestFocus(muserEmailAddress);
        }
        if (pass.equals("")) {
            mlayoutpassword.setError(this.getResources().getString(R.string.enterPasswordErrMsg));
            requestFocus(muserPassword);
            logInCheck = false;
        } else {
            mlayoutpassword.setError(null);
            requestFocus(muserPassword);
        }
        return logInCheck;

    }
    private boolean validEmail(String email) {
        String email_pattern = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(email_pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /**
     * forgotDialog() method is used to user forgot password then user given your password then this method is called
     */
    private void forgotDialog() {

        final MaterialDialog forgotDialog = new MaterialDialog.Builder(Login.this)
                .autoDismiss(false)
                .customView(R.layout.activity_forgot_password, true)
                .negativeText(Login.this.getResources().getString(android.R.string.cancel))
                .positiveText(Login.this.getResources().getString(android.R.string.ok))
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {

                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(final MaterialDialog dialog, DialogAction which) {
                        if (dialogValidation()) {
                            forgotPassword();

                            userForgotPassword.setFocusable(false);
                            userForgotPassword.setFocusableInTouchMode(false);
                            userForgotPassword.setClickable(false);

                            dialog.dismiss();
                        } else {

                        }
                    }

                }).build();
        mTilForgotPassword = (TextInputLayout) forgotDialog.getCustomView().findViewById(R.id.til_forgot_password);
        userForgotPassword = (EditText) forgotDialog.getCustomView().findViewById(R.id.et_forgot_pass_email);

        forgotDialog.show();
        forgotDialog.setCanceledOnTouchOutside(false);
        forgotDialog.setCancelable(false);
    }

    /**
     *
     */
    private void forgotPassword()
    {
        String mEmail=userForgotPassword.getText().toString();
        // Start the ProgressDailog
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
        mLoading = true;
        supportInvalidateOptionsMenu();
        setUpComponents(false);
        setButtonEnable(false);
        RequestBody requestBody = new FormBody.Builder()
                .add(BucketListConstants.CUSTOMER_EMAIL, mEmail)
                .build();

        try {
            final Request request = new Request.Builder()
                    .url("https://poojathakor99.000webhostapp.com/BucketList/android_forgot.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {

                @Override
                public void onFailure(Call call, final IOException e) {
                    Log.e("productFragement", "onFailure", e);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // stop showing progress Bar

                            mLoading = false;
                            supportInvalidateOptionsMenu();
                            setUpComponents(true);
                            setButtonEnable(true);
                            Toast.makeText(Login.this,
                                    getResources().getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();


                        }
                    });
                } // onFailure ends here

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("response", " " + response);
                    Log.e("response.body", " " + response.body());

                    if (response.body() != null) {
                        final String result = response.body().string();
                        try {
                            final JSONObject jsonObj = new JSONObject(result);
                            Log.e("Inside onResponse Try", result);
                            final String mMessage = jsonObj.getString(BucketListConstants.MESSAGE);
                            final boolean auth=jsonObj.getBoolean(BucketListConstants.AUTH);

                            Log.e("Mstatus ", "" + mMessage);

                            // if mStatus matches the server status
                            if (auth==true)
                            {

                                // show the message in either case
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Login.this, mMessage, Toast.LENGTH_SHORT).show();
                                        // Take user to Menu activity

                                    }
                                });
                            } else
                            {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        // stop displaying Loader
                                        mLoading = false;
                                        supportInvalidateOptionsMenu();
                                        setUpComponents(true);
                                        setButtonEnable(true);
                                        Toast.makeText(Login.this, mMessage, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                        } // try insde onResponse ends here
                        catch (final JSONException e) {
                            Log.e("productFragement", "Inside on Response try", e);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // stop displaying Loader
                                    mLoading = false;
                                    supportInvalidateOptionsMenu();
                                    setUpComponents(true);
                                    setButtonEnable(true);
                                    Toast.makeText(Login.this, getResources()
                                            .getString(R.string.msg_something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } // Catch inside onResponse ends here
                    } // if condition on request body ends here
                } // onResponse ends here
            }); // CallBack Ends here
        }
        catch (Exception ex)
        {
            Log.e("SplashActivity", "ex", ex);
        }
    } // forgot password ends here

    /**
     * used to validate the entered value in Forgot-password dialog.
     */
    private boolean dialogValidation() {

        String email_pattern = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        Pattern pattern = Pattern.compile(email_pattern);
        Matcher matcher = pattern.matcher(userForgotPassword.getText().toString());

        if (!(matcher.matches())) {
            mTilForgotPassword.setError(Login.this.getResources().getString(R.string.enterSignIdErrMsg));
            return false;
        }

        return matcher.matches();
    }
    private void setFontsForText() {

        /**
         * Changing the font face, font size, font color, action bar title
         * using the TypeFace method of android
         */
        String fontRalewayBold = "fonts/Raleway-Bold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontRalewayBold);
        //titleAccount.setTypeface(tf);
        msignup.setTypeface(tf);
        mforgotpasswor.setTypeface(tf);
        mhideshowPassword.setTypeface(tf);
        mhideshowPassword.setTypeface(tf);


        String fontRalewayLight = "fonts/Raleway-Regular.ttf";
        Typeface type = Typeface.createFromAsset(getAssets(), fontRalewayLight);
        muserEmailAddress.setTypeface(type);
        muserPassword.setTypeface(type);

    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    /**
     * showHidePassword() method is used to password field in show and hide password perform
     */
    private void showHidePassword() {


        // show password

        if (isPasswordVisible) {
            isPasswordVisible = false;
            mhideshowPassword.setText(R.string.tv_hide_password);
            muserPassword.setTransformationMethod(null);
            muserPassword.setSelection(muserPassword.getText().length());
            mhideshowPassword.setVisibility(View.GONE);
            mhideshowPassword.setVisibility(View.VISIBLE);

        } else {

            //Hide password
            isPasswordVisible = true;
            mhideshowPassword.setText(R.string.tv_show_password);
            muserPassword.setTransformationMethod(new PasswordTransformationMethod());
            muserPassword.setSelection(muserPassword.getText().length());
            mhideshowPassword.setVisibility(View.GONE);
            mhideshowPassword.setVisibility(View.VISIBLE);
        }
    } //showHidePassword ends here

    public void setUpComponents(boolean flag) {
        muserLogin.setClickable(flag);
        mforgotpasswor.setClickable(flag);
        msignup.setClickable(flag);


    }


} // main class ends here
