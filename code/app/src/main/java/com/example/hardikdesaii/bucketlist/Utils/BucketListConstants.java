package com.example.hardikdesaii.bucketlist.Utils;

/**
 * Created by HardikDesaii on 12/05/17.
 */

public class BucketListConstants
{

    public static final String USER_FIRSTNAME ="fname";
    public static final String USER_LASTNAME ="lname" ;
    public static final String CUSTOMER_EMAIL ="email" ;
    public static final String CUSTOMER_PASSWORD ="pwd" ;
    public static final String MESSAGE ="msg" ;
    public static final String AUTH ="auth" ;

    public static final String BIRTH_DATE ="bday" ;
    public static final String FCM_KEY ="fcmkey" ;
    public static final String WISH ="wish" ;
}
